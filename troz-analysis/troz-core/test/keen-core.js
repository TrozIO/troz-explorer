(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
(function(env){
  var previousKeen = env.Keen || undefined;
  var each = require('./utils/each'),
      extend = require('./utils/extend'),
      parseParams = require('./utils/parse-params'),
      serialize = require('./utils/serialize');

  var Emitter = require('component-emitter');

  function Client(props){
    if (this instanceof Client === false) {
      return new Client(props);
    }
    this.configure(props);

    // Set up event handling
    if (Client.debug) {
      this.on('error', Client.log);
    }
    this.emit('ready');
    Client.emit('client', this);
  }

  if (previousKeen && typeof previousKeen.resources === 'undefined') {
    Client.legacyVersion = previousKeen;
  }

  Emitter(Client);
  Emitter(Client.prototype);

  extend(Client, {
    debug: false,
    enabled: true,
    loaded: false,
    version: '__VERSION__'
  });

  // Set or extend helpers
  Client.helpers = Client.helpers || {};

  // Set or extend resources
  Client.resources = Client.resources || {};
  extend(Client.resources, {
    'base'      : '{protocol}://{host}',
    'version'   : '{protocol}://{host}/{version}',
    'projects'  : '{protocol}://{host}/{version}/projects',
    'projectId' : '{protocol}://{host}/{version}/projects/{projectId}',
    'events'    : '{protocol}://{host}/{version}/projects/{projectId}/events',
    'queries'   : '{protocol}://{host}/{version}/projects/{projectId}/queries'
  });

  // Set or extend utils
  Client.utils = Client.utils || {};
  extend(Client.utils, {
    'each'        : each,
    'extend'      : extend,
    'parseParams' : parseParams,
    'serialize'   : serialize
  });

  Client.extendLibrary = function(target, source) {
    var previous = previousKeen || source;
    if (isDefined(previous) && isDefined(previous.resources)) {
      each(previous, function(value, key) {
        if (typeof value === 'object') {
          target[key] = target[key] || {};
          extend(target[key], value);
        }
        else {
          target[key] = target[key] || value;
        }
      });
      extend(target.prototype, previous.prototype);
    }
    return target;
  };

  Client.log = function(str){
    if (Client.debug && typeof console === 'object') {
      console.log('[Keen]', str);
    }
  };

  Client.noConflict = function(){
    if (typeof env.Keen !== 'undefined') {
      env.Keen = Client.legacyVersion || previousKeen;
    }
    return Client;
  };

  Client.ready = function(fn){
    if (Client.loaded) {
      fn();
    }
    else {
      Client.once('ready', fn);
    }
  };

  Client.prototype.configure = function(obj){
    var config = obj || {};
    this.config = this.config || {
      projectId    : undefined,
      writeKey     : undefined,
      host         : 'api.keen.io',
      version      : '3.0',
      protocol     : 'https',
      requestType  : 'jsonp',
      resources    : extend({}, Client.resources)
    };

    // IE<10 request shim
    if (typeof window !== 'undefined' && window.navigator.userAgent.indexOf('MSIE') > -1) {
      config.protocol = document.location.protocol.replace(':', '');
    }

    if (config.host) {
      config.host.replace(/.*?:\/\//g, '');
    }

    extend(this.config, config);
    return this;
  };

  Client.prototype.masterKey = function(str){
    if (!arguments.length) return this.config.masterKey;
    this.config.masterKey = str ? String(str) : null;
    return this;
  };

  Client.prototype.projectId = function(str){
    if (!arguments.length) return this.config.projectId;
    this.config.projectId = (str ? String(str) : null);
    return this;
  };

  Client.prototype.resources = function(obj){
    if (!arguments.length) return this.config.resources;
    var self = this;
    if (typeof obj === 'object') {
      each(obj, function(value, key){
        self.config.resources[key] = (value ? value : null);
      });
    }
    return self;
  };

  Client.prototype.url = function(name){
    var args = Array.prototype.slice.call(arguments, 1),
        baseUrl = this.config.resources.base || '{protocol}://{host}',
        path;

    if (name && typeof name === 'string') {
      if (this.config.resources[name]) {
        path = this.config.resources[name];
      }
      else {
        path = baseUrl + name;
      }
    }
    else {
      path = baseUrl;
    }

    each(this.config, function(value, key){
      if (typeof value !== 'object') {
        path = path.replace('{' + key + '}', value);
      }
    });

    each(args, function(arg, i){
      if (typeof arg === 'string') {
        path += '/' + arg;
      }
      else if (typeof arg === 'object') {
        path += '?';
        each(arg, function(value, key){
          path += key + '=' + value + '&';
        });
        path = path.slice(0, -1);
      }
    });

    return path;
  };

  domReady(function(){
    Client.loaded = true;
    Client.emit('ready');
  });

  function domReady(fn){
    if (Client.loaded || typeof document === 'undefined') {
      fn();
      return;
    }
    // Firefox 3.5 shim
    if(document.readyState == null && document.addEventListener){
      document.addEventListener('DOMContentLoaded', function DOMContentLoaded(){
        document.removeEventListener('DOMContentLoaded', DOMContentLoaded, false);
        document.readyState = 'complete';
      }, false);
      document.readyState = 'loading';
    }
    testDom(fn);
  }

  function testDom(fn){
    if (/in/.test(document.readyState)) {
      setTimeout(function(){
        testDom(fn);
      }, 9);
    }
    else {
      fn();
    }
  }

  function isDefined(target) {
    return typeof target !== 'undefined';
  }

  function isUndefined(target) {
    return typeof target === 'undefined';
  }

  module.exports = Client;

}).call(this, typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./utils/each":2,"./utils/extend":3,"./utils/parse-params":4,"./utils/serialize":5,"component-emitter":6}],2:[function(require,module,exports){
module.exports = each;

function each(o, cb, s){
  var n;
  if (!o){
    return 0;
  }
  s = !s ? o : s;
  if (o instanceof Array){
    // Indexed arrays, needed for Safari
    for (n=0; n<o.length; n++) {
      if (cb.call(s, o[n], n, o) === false){
        return 0;
      }
    }
  } else {
    // Hashtables
    for (n in o){
      if (o.hasOwnProperty(n)) {
        if (cb.call(s, o[n], n, o) === false){
          return 0;
        }
      }
    }
  }
  return 1;
}

},{}],3:[function(require,module,exports){
module.exports = extend;

function extend(target){
  for (var i = 1; i < arguments.length; i++) {
    for (var prop in arguments[i]){
      target[prop] = arguments[i][prop];
    }
  }
  return target;
};

},{}],4:[function(require,module,exports){
module.exports = parseParams;

function parseParams(str){
  // via: http://stackoverflow.com/a/2880929/2511985
  var urlParams = {},
      match,
      pl     = /\+/g,  // Regex for replacing addition symbol with a space
      search = /([^&=]+)=?([^&]*)/g,
      decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
      query  = str.split("?")[1];

  while (!!(match=search.exec(query))) {
    urlParams[decode(match[1])] = decode(match[2]);
  }
  return urlParams;
};

},{}],5:[function(require,module,exports){
var each = require('./each'),
    extend = require('./extend');

module.exports = serialize;

function serialize(data){
  var query = [];
  each(data, function(value, key){
    if ('string' !== typeof value) {
      value = JSON.stringify(value);
    }
    query.push(key + '=' + encodeURIComponent(value));
  });
  return query.join('&');
}

},{"./each":2,"./extend":3}],6:[function(require,module,exports){

/**
 * Expose `Emitter`.
 */

if (typeof module !== 'undefined') {
  module.exports = Emitter;
}

/**
 * Initialize a new `Emitter`.
 *
 * @api public
 */

function Emitter(obj) {
  if (obj) return mixin(obj);
};

/**
 * Mixin the emitter properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */

function mixin(obj) {
  for (var key in Emitter.prototype) {
    obj[key] = Emitter.prototype[key];
  }
  return obj;
}

/**
 * Listen on the given `event` with `fn`.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.on =
Emitter.prototype.addEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};
  (this._callbacks['$' + event] = this._callbacks['$' + event] || [])
    .push(fn);
  return this;
};

/**
 * Adds an `event` listener that will be invoked a single
 * time then automatically removed.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.once = function(event, fn){
  function on() {
    this.off(event, on);
    fn.apply(this, arguments);
  }

  on.fn = fn;
  this.on(event, on);
  return this;
};

/**
 * Remove the given callback for `event` or all
 * registered callbacks.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.off =
Emitter.prototype.removeListener =
Emitter.prototype.removeAllListeners =
Emitter.prototype.removeEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};

  // all
  if (0 == arguments.length) {
    this._callbacks = {};
    return this;
  }

  // specific event
  var callbacks = this._callbacks['$' + event];
  if (!callbacks) return this;

  // remove all handlers
  if (1 == arguments.length) {
    delete this._callbacks['$' + event];
    return this;
  }

  // remove specific handler
  var cb;
  for (var i = 0; i < callbacks.length; i++) {
    cb = callbacks[i];
    if (cb === fn || cb.fn === fn) {
      callbacks.splice(i, 1);
      break;
    }
  }
  return this;
};

/**
 * Emit `event` with the given args.
 *
 * @param {String} event
 * @param {Mixed} ...
 * @return {Emitter}
 */

Emitter.prototype.emit = function(event){
  this._callbacks = this._callbacks || {};
  var args = [].slice.call(arguments, 1)
    , callbacks = this._callbacks['$' + event];

  if (callbacks) {
    callbacks = callbacks.slice(0);
    for (var i = 0, len = callbacks.length; i < len; ++i) {
      callbacks[i].apply(this, args);
    }
  }

  return this;
};

/**
 * Return array of callbacks for `event`.
 *
 * @param {String} event
 * @return {Array}
 * @api public
 */

Emitter.prototype.listeners = function(event){
  this._callbacks = this._callbacks || {};
  return this._callbacks['$' + event] || [];
};

/**
 * Check if this emitter has `event` handlers.
 *
 * @param {String} event
 * @return {Boolean}
 * @api public
 */

Emitter.prototype.hasListeners = function(event){
  return !! this.listeners(event).length;
};

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJsaWIvaW5kZXguanMiLCJsaWIvdXRpbHMvZWFjaC5qcyIsImxpYi91dGlscy9leHRlbmQuanMiLCJsaWIvdXRpbHMvcGFyc2UtcGFyYW1zLmpzIiwibGliL3V0aWxzL3NlcmlhbGl6ZS5qcyIsIm5vZGVfbW9kdWxlcy9jb21wb25lbnQtZW1pdHRlci9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FDck9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDaEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiKGZ1bmN0aW9uKGVudil7XG4gIHZhciBwcmV2aW91c0tlZW4gPSBlbnYuS2VlbiB8fCB1bmRlZmluZWQ7XG4gIHZhciBlYWNoID0gcmVxdWlyZSgnLi91dGlscy9lYWNoJyksXG4gICAgICBleHRlbmQgPSByZXF1aXJlKCcuL3V0aWxzL2V4dGVuZCcpLFxuICAgICAgcGFyc2VQYXJhbXMgPSByZXF1aXJlKCcuL3V0aWxzL3BhcnNlLXBhcmFtcycpLFxuICAgICAgc2VyaWFsaXplID0gcmVxdWlyZSgnLi91dGlscy9zZXJpYWxpemUnKTtcblxuICB2YXIgRW1pdHRlciA9IHJlcXVpcmUoJ2NvbXBvbmVudC1lbWl0dGVyJyk7XG5cbiAgZnVuY3Rpb24gQ2xpZW50KHByb3BzKXtcbiAgICBpZiAodGhpcyBpbnN0YW5jZW9mIENsaWVudCA9PT0gZmFsc2UpIHtcbiAgICAgIHJldHVybiBuZXcgQ2xpZW50KHByb3BzKTtcbiAgICB9XG4gICAgdGhpcy5jb25maWd1cmUocHJvcHMpO1xuXG4gICAgLy8gU2V0IHVwIGV2ZW50IGhhbmRsaW5nXG4gICAgaWYgKENsaWVudC5kZWJ1Zykge1xuICAgICAgdGhpcy5vbignZXJyb3InLCBDbGllbnQubG9nKTtcbiAgICB9XG4gICAgdGhpcy5lbWl0KCdyZWFkeScpO1xuICAgIENsaWVudC5lbWl0KCdjbGllbnQnLCB0aGlzKTtcbiAgfVxuXG4gIGlmIChwcmV2aW91c0tlZW4gJiYgdHlwZW9mIHByZXZpb3VzS2Vlbi5yZXNvdXJjZXMgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgQ2xpZW50LmxlZ2FjeVZlcnNpb24gPSBwcmV2aW91c0tlZW47XG4gIH1cblxuICBFbWl0dGVyKENsaWVudCk7XG4gIEVtaXR0ZXIoQ2xpZW50LnByb3RvdHlwZSk7XG5cbiAgZXh0ZW5kKENsaWVudCwge1xuICAgIGRlYnVnOiBmYWxzZSxcbiAgICBlbmFibGVkOiB0cnVlLFxuICAgIGxvYWRlZDogZmFsc2UsXG4gICAgdmVyc2lvbjogJ19fVkVSU0lPTl9fJ1xuICB9KTtcblxuICAvLyBTZXQgb3IgZXh0ZW5kIGhlbHBlcnNcbiAgQ2xpZW50LmhlbHBlcnMgPSBDbGllbnQuaGVscGVycyB8fCB7fTtcblxuICAvLyBTZXQgb3IgZXh0ZW5kIHJlc291cmNlc1xuICBDbGllbnQucmVzb3VyY2VzID0gQ2xpZW50LnJlc291cmNlcyB8fCB7fTtcbiAgZXh0ZW5kKENsaWVudC5yZXNvdXJjZXMsIHtcbiAgICAnYmFzZScgICAgICA6ICd7cHJvdG9jb2x9Oi8ve2hvc3R9JyxcbiAgICAndmVyc2lvbicgICA6ICd7cHJvdG9jb2x9Oi8ve2hvc3R9L3t2ZXJzaW9ufScsXG4gICAgJ3Byb2plY3RzJyAgOiAne3Byb3RvY29sfTovL3tob3N0fS97dmVyc2lvbn0vcHJvamVjdHMnLFxuICAgICdwcm9qZWN0SWQnIDogJ3twcm90b2NvbH06Ly97aG9zdH0ve3ZlcnNpb259L3Byb2plY3RzL3twcm9qZWN0SWR9JyxcbiAgICAnZXZlbnRzJyAgICA6ICd7cHJvdG9jb2x9Oi8ve2hvc3R9L3t2ZXJzaW9ufS9wcm9qZWN0cy97cHJvamVjdElkfS9ldmVudHMnLFxuICAgICdxdWVyaWVzJyAgIDogJ3twcm90b2NvbH06Ly97aG9zdH0ve3ZlcnNpb259L3Byb2plY3RzL3twcm9qZWN0SWR9L3F1ZXJpZXMnXG4gIH0pO1xuXG4gIC8vIFNldCBvciBleHRlbmQgdXRpbHNcbiAgQ2xpZW50LnV0aWxzID0gQ2xpZW50LnV0aWxzIHx8IHt9O1xuICBleHRlbmQoQ2xpZW50LnV0aWxzLCB7XG4gICAgJ2VhY2gnICAgICAgICA6IGVhY2gsXG4gICAgJ2V4dGVuZCcgICAgICA6IGV4dGVuZCxcbiAgICAncGFyc2VQYXJhbXMnIDogcGFyc2VQYXJhbXMsXG4gICAgJ3NlcmlhbGl6ZScgICA6IHNlcmlhbGl6ZVxuICB9KTtcblxuICBDbGllbnQuZXh0ZW5kTGlicmFyeSA9IGZ1bmN0aW9uKHRhcmdldCwgc291cmNlKSB7XG4gICAgdmFyIHByZXZpb3VzID0gcHJldmlvdXNLZWVuIHx8IHNvdXJjZTtcbiAgICBpZiAoaXNEZWZpbmVkKHByZXZpb3VzKSAmJiBpc0RlZmluZWQocHJldmlvdXMucmVzb3VyY2VzKSkge1xuICAgICAgZWFjaChwcmV2aW91cywgZnVuY3Rpb24odmFsdWUsIGtleSkge1xuICAgICAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgIHRhcmdldFtrZXldID0gdGFyZ2V0W2tleV0gfHwge307XG4gICAgICAgICAgZXh0ZW5kKHRhcmdldFtrZXldLCB2YWx1ZSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgdGFyZ2V0W2tleV0gPSB0YXJnZXRba2V5XSB8fCB2YWx1ZTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICBleHRlbmQodGFyZ2V0LnByb3RvdHlwZSwgcHJldmlvdXMucHJvdG90eXBlKTtcbiAgICB9XG4gICAgcmV0dXJuIHRhcmdldDtcbiAgfTtcblxuICBDbGllbnQubG9nID0gZnVuY3Rpb24oc3RyKXtcbiAgICBpZiAoQ2xpZW50LmRlYnVnICYmIHR5cGVvZiBjb25zb2xlID09PSAnb2JqZWN0Jykge1xuICAgICAgY29uc29sZS5sb2coJ1tLZWVuXScsIHN0cik7XG4gICAgfVxuICB9O1xuXG4gIENsaWVudC5ub0NvbmZsaWN0ID0gZnVuY3Rpb24oKXtcbiAgICBpZiAodHlwZW9mIGVudi5LZWVuICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgZW52LktlZW4gPSBDbGllbnQubGVnYWN5VmVyc2lvbiB8fCBwcmV2aW91c0tlZW47XG4gICAgfVxuICAgIHJldHVybiBDbGllbnQ7XG4gIH07XG5cbiAgQ2xpZW50LnJlYWR5ID0gZnVuY3Rpb24oZm4pe1xuICAgIGlmIChDbGllbnQubG9hZGVkKSB7XG4gICAgICBmbigpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIENsaWVudC5vbmNlKCdyZWFkeScsIGZuKTtcbiAgICB9XG4gIH07XG5cbiAgQ2xpZW50LnByb3RvdHlwZS5jb25maWd1cmUgPSBmdW5jdGlvbihvYmope1xuICAgIHZhciBjb25maWcgPSBvYmogfHwge307XG4gICAgdGhpcy5jb25maWcgPSB0aGlzLmNvbmZpZyB8fCB7XG4gICAgICBwcm9qZWN0SWQgICAgOiB1bmRlZmluZWQsXG4gICAgICB3cml0ZUtleSAgICAgOiB1bmRlZmluZWQsXG4gICAgICBob3N0ICAgICAgICAgOiAnYXBpLmtlZW4uaW8nLFxuICAgICAgdmVyc2lvbiAgICAgIDogJzMuMCcsXG4gICAgICBwcm90b2NvbCAgICAgOiAnaHR0cHMnLFxuICAgICAgcmVxdWVzdFR5cGUgIDogJ2pzb25wJyxcbiAgICAgIHJlc291cmNlcyAgICA6IGV4dGVuZCh7fSwgQ2xpZW50LnJlc291cmNlcylcbiAgICB9O1xuXG4gICAgLy8gSUU8MTAgcmVxdWVzdCBzaGltXG4gICAgaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoJ01TSUUnKSA+IC0xKSB7XG4gICAgICBjb25maWcucHJvdG9jb2wgPSBkb2N1bWVudC5sb2NhdGlvbi5wcm90b2NvbC5yZXBsYWNlKCc6JywgJycpO1xuICAgIH1cblxuICAgIGlmIChjb25maWcuaG9zdCkge1xuICAgICAgY29uZmlnLmhvc3QucmVwbGFjZSgvLio/OlxcL1xcLy9nLCAnJyk7XG4gICAgfVxuXG4gICAgZXh0ZW5kKHRoaXMuY29uZmlnLCBjb25maWcpO1xuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gIENsaWVudC5wcm90b3R5cGUubWFzdGVyS2V5ID0gZnVuY3Rpb24oc3RyKXtcbiAgICBpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHJldHVybiB0aGlzLmNvbmZpZy5tYXN0ZXJLZXk7XG4gICAgdGhpcy5jb25maWcubWFzdGVyS2V5ID0gc3RyID8gU3RyaW5nKHN0cikgOiBudWxsO1xuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gIENsaWVudC5wcm90b3R5cGUucHJvamVjdElkID0gZnVuY3Rpb24oc3RyKXtcbiAgICBpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHJldHVybiB0aGlzLmNvbmZpZy5wcm9qZWN0SWQ7XG4gICAgdGhpcy5jb25maWcucHJvamVjdElkID0gKHN0ciA/IFN0cmluZyhzdHIpIDogbnVsbCk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH07XG5cbiAgQ2xpZW50LnByb3RvdHlwZS5yZXNvdXJjZXMgPSBmdW5jdGlvbihvYmope1xuICAgIGlmICghYXJndW1lbnRzLmxlbmd0aCkgcmV0dXJuIHRoaXMuY29uZmlnLnJlc291cmNlcztcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgaWYgKHR5cGVvZiBvYmogPT09ICdvYmplY3QnKSB7XG4gICAgICBlYWNoKG9iaiwgZnVuY3Rpb24odmFsdWUsIGtleSl7XG4gICAgICAgIHNlbGYuY29uZmlnLnJlc291cmNlc1trZXldID0gKHZhbHVlID8gdmFsdWUgOiBudWxsKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgICByZXR1cm4gc2VsZjtcbiAgfTtcblxuICBDbGllbnQucHJvdG90eXBlLnVybCA9IGZ1bmN0aW9uKG5hbWUpe1xuICAgIHZhciBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcbiAgICAgICAgYmFzZVVybCA9IHRoaXMuY29uZmlnLnJlc291cmNlcy5iYXNlIHx8ICd7cHJvdG9jb2x9Oi8ve2hvc3R9JyxcbiAgICAgICAgcGF0aDtcblxuICAgIGlmIChuYW1lICYmIHR5cGVvZiBuYW1lID09PSAnc3RyaW5nJykge1xuICAgICAgaWYgKHRoaXMuY29uZmlnLnJlc291cmNlc1tuYW1lXSkge1xuICAgICAgICBwYXRoID0gdGhpcy5jb25maWcucmVzb3VyY2VzW25hbWVdO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIHBhdGggPSBiYXNlVXJsICsgbmFtZTtcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBwYXRoID0gYmFzZVVybDtcbiAgICB9XG5cbiAgICBlYWNoKHRoaXMuY29uZmlnLCBmdW5jdGlvbih2YWx1ZSwga2V5KXtcbiAgICAgIGlmICh0eXBlb2YgdmFsdWUgIT09ICdvYmplY3QnKSB7XG4gICAgICAgIHBhdGggPSBwYXRoLnJlcGxhY2UoJ3snICsga2V5ICsgJ30nLCB2YWx1ZSk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBlYWNoKGFyZ3MsIGZ1bmN0aW9uKGFyZywgaSl7XG4gICAgICBpZiAodHlwZW9mIGFyZyA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgcGF0aCArPSAnLycgKyBhcmc7XG4gICAgICB9XG4gICAgICBlbHNlIGlmICh0eXBlb2YgYXJnID09PSAnb2JqZWN0Jykge1xuICAgICAgICBwYXRoICs9ICc/JztcbiAgICAgICAgZWFjaChhcmcsIGZ1bmN0aW9uKHZhbHVlLCBrZXkpe1xuICAgICAgICAgIHBhdGggKz0ga2V5ICsgJz0nICsgdmFsdWUgKyAnJic7XG4gICAgICAgIH0pO1xuICAgICAgICBwYXRoID0gcGF0aC5zbGljZSgwLCAtMSk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICByZXR1cm4gcGF0aDtcbiAgfTtcblxuICBkb21SZWFkeShmdW5jdGlvbigpe1xuICAgIENsaWVudC5sb2FkZWQgPSB0cnVlO1xuICAgIENsaWVudC5lbWl0KCdyZWFkeScpO1xuICB9KTtcblxuICBmdW5jdGlvbiBkb21SZWFkeShmbil7XG4gICAgaWYgKENsaWVudC5sb2FkZWQgfHwgdHlwZW9mIGRvY3VtZW50ID09PSAndW5kZWZpbmVkJykge1xuICAgICAgZm4oKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgLy8gRmlyZWZveCAzLjUgc2hpbVxuICAgIGlmKGRvY3VtZW50LnJlYWR5U3RhdGUgPT0gbnVsbCAmJiBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKXtcbiAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBmdW5jdGlvbiBET01Db250ZW50TG9hZGVkKCl7XG4gICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBET01Db250ZW50TG9hZGVkLCBmYWxzZSk7XG4gICAgICAgIGRvY3VtZW50LnJlYWR5U3RhdGUgPSAnY29tcGxldGUnO1xuICAgICAgfSwgZmFsc2UpO1xuICAgICAgZG9jdW1lbnQucmVhZHlTdGF0ZSA9ICdsb2FkaW5nJztcbiAgICB9XG4gICAgdGVzdERvbShmbik7XG4gIH1cblxuICBmdW5jdGlvbiB0ZXN0RG9tKGZuKXtcbiAgICBpZiAoL2luLy50ZXN0KGRvY3VtZW50LnJlYWR5U3RhdGUpKSB7XG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgIHRlc3REb20oZm4pO1xuICAgICAgfSwgOSk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgZm4oKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBpc0RlZmluZWQodGFyZ2V0KSB7XG4gICAgcmV0dXJuIHR5cGVvZiB0YXJnZXQgIT09ICd1bmRlZmluZWQnO1xuICB9XG5cbiAgZnVuY3Rpb24gaXNVbmRlZmluZWQodGFyZ2V0KSB7XG4gICAgcmV0dXJuIHR5cGVvZiB0YXJnZXQgPT09ICd1bmRlZmluZWQnO1xuICB9XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSBDbGllbnQ7XG5cbn0pLmNhbGwodGhpcywgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgPyB3aW5kb3cgOiB0eXBlb2YgZ2xvYmFsICE9PSAndW5kZWZpbmVkJyA/IGdsb2JhbCA6IHR5cGVvZiBzZWxmICE9PSAndW5kZWZpbmVkJyA/IHNlbGYgOiB7fSk7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGVhY2g7XG5cbmZ1bmN0aW9uIGVhY2gobywgY2IsIHMpe1xuICB2YXIgbjtcbiAgaWYgKCFvKXtcbiAgICByZXR1cm4gMDtcbiAgfVxuICBzID0gIXMgPyBvIDogcztcbiAgaWYgKG8gaW5zdGFuY2VvZiBBcnJheSl7XG4gICAgLy8gSW5kZXhlZCBhcnJheXMsIG5lZWRlZCBmb3IgU2FmYXJpXG4gICAgZm9yIChuPTA7IG48by5sZW5ndGg7IG4rKykge1xuICAgICAgaWYgKGNiLmNhbGwocywgb1tuXSwgbiwgbykgPT09IGZhbHNlKXtcbiAgICAgICAgcmV0dXJuIDA7XG4gICAgICB9XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIC8vIEhhc2h0YWJsZXNcbiAgICBmb3IgKG4gaW4gbyl7XG4gICAgICBpZiAoby5oYXNPd25Qcm9wZXJ0eShuKSkge1xuICAgICAgICBpZiAoY2IuY2FsbChzLCBvW25dLCBuLCBvKSA9PT0gZmFsc2Upe1xuICAgICAgICAgIHJldHVybiAwO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG4gIHJldHVybiAxO1xufVxuIiwibW9kdWxlLmV4cG9ydHMgPSBleHRlbmQ7XG5cbmZ1bmN0aW9uIGV4dGVuZCh0YXJnZXQpe1xuICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgIGZvciAodmFyIHByb3AgaW4gYXJndW1lbnRzW2ldKXtcbiAgICAgIHRhcmdldFtwcm9wXSA9IGFyZ3VtZW50c1tpXVtwcm9wXTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHRhcmdldDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHBhcnNlUGFyYW1zO1xuXG5mdW5jdGlvbiBwYXJzZVBhcmFtcyhzdHIpe1xuICAvLyB2aWE6IGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzI4ODA5MjkvMjUxMTk4NVxuICB2YXIgdXJsUGFyYW1zID0ge30sXG4gICAgICBtYXRjaCxcbiAgICAgIHBsICAgICA9IC9cXCsvZywgIC8vIFJlZ2V4IGZvciByZXBsYWNpbmcgYWRkaXRpb24gc3ltYm9sIHdpdGggYSBzcGFjZVxuICAgICAgc2VhcmNoID0gLyhbXiY9XSspPT8oW14mXSopL2csXG4gICAgICBkZWNvZGUgPSBmdW5jdGlvbiAocykgeyByZXR1cm4gZGVjb2RlVVJJQ29tcG9uZW50KHMucmVwbGFjZShwbCwgXCIgXCIpKTsgfSxcbiAgICAgIHF1ZXJ5ICA9IHN0ci5zcGxpdChcIj9cIilbMV07XG5cbiAgd2hpbGUgKCEhKG1hdGNoPXNlYXJjaC5leGVjKHF1ZXJ5KSkpIHtcbiAgICB1cmxQYXJhbXNbZGVjb2RlKG1hdGNoWzFdKV0gPSBkZWNvZGUobWF0Y2hbMl0pO1xuICB9XG4gIHJldHVybiB1cmxQYXJhbXM7XG59O1xuIiwidmFyIGVhY2ggPSByZXF1aXJlKCcuL2VhY2gnKSxcbiAgICBleHRlbmQgPSByZXF1aXJlKCcuL2V4dGVuZCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHNlcmlhbGl6ZTtcblxuZnVuY3Rpb24gc2VyaWFsaXplKGRhdGEpe1xuICB2YXIgcXVlcnkgPSBbXTtcbiAgZWFjaChkYXRhLCBmdW5jdGlvbih2YWx1ZSwga2V5KXtcbiAgICBpZiAoJ3N0cmluZycgIT09IHR5cGVvZiB2YWx1ZSkge1xuICAgICAgdmFsdWUgPSBKU09OLnN0cmluZ2lmeSh2YWx1ZSk7XG4gICAgfVxuICAgIHF1ZXJ5LnB1c2goa2V5ICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHZhbHVlKSk7XG4gIH0pO1xuICByZXR1cm4gcXVlcnkuam9pbignJicpO1xufVxuIiwiXHJcbi8qKlxyXG4gKiBFeHBvc2UgYEVtaXR0ZXJgLlxyXG4gKi9cclxuXHJcbmlmICh0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJykge1xyXG4gIG1vZHVsZS5leHBvcnRzID0gRW1pdHRlcjtcclxufVxyXG5cclxuLyoqXHJcbiAqIEluaXRpYWxpemUgYSBuZXcgYEVtaXR0ZXJgLlxyXG4gKlxyXG4gKiBAYXBpIHB1YmxpY1xyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIEVtaXR0ZXIob2JqKSB7XHJcbiAgaWYgKG9iaikgcmV0dXJuIG1peGluKG9iaik7XHJcbn07XHJcblxyXG4vKipcclxuICogTWl4aW4gdGhlIGVtaXR0ZXIgcHJvcGVydGllcy5cclxuICpcclxuICogQHBhcmFtIHtPYmplY3R9IG9ialxyXG4gKiBAcmV0dXJuIHtPYmplY3R9XHJcbiAqIEBhcGkgcHJpdmF0ZVxyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIG1peGluKG9iaikge1xyXG4gIGZvciAodmFyIGtleSBpbiBFbWl0dGVyLnByb3RvdHlwZSkge1xyXG4gICAgb2JqW2tleV0gPSBFbWl0dGVyLnByb3RvdHlwZVtrZXldO1xyXG4gIH1cclxuICByZXR1cm4gb2JqO1xyXG59XHJcblxyXG4vKipcclxuICogTGlzdGVuIG9uIHRoZSBnaXZlbiBgZXZlbnRgIHdpdGggYGZuYC5cclxuICpcclxuICogQHBhcmFtIHtTdHJpbmd9IGV2ZW50XHJcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZuXHJcbiAqIEByZXR1cm4ge0VtaXR0ZXJ9XHJcbiAqIEBhcGkgcHVibGljXHJcbiAqL1xyXG5cclxuRW1pdHRlci5wcm90b3R5cGUub24gPVxyXG5FbWl0dGVyLnByb3RvdHlwZS5hZGRFdmVudExpc3RlbmVyID0gZnVuY3Rpb24oZXZlbnQsIGZuKXtcclxuICB0aGlzLl9jYWxsYmFja3MgPSB0aGlzLl9jYWxsYmFja3MgfHwge307XHJcbiAgKHRoaXMuX2NhbGxiYWNrc1snJCcgKyBldmVudF0gPSB0aGlzLl9jYWxsYmFja3NbJyQnICsgZXZlbnRdIHx8IFtdKVxyXG4gICAgLnB1c2goZm4pO1xyXG4gIHJldHVybiB0aGlzO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIEFkZHMgYW4gYGV2ZW50YCBsaXN0ZW5lciB0aGF0IHdpbGwgYmUgaW52b2tlZCBhIHNpbmdsZVxyXG4gKiB0aW1lIHRoZW4gYXV0b21hdGljYWxseSByZW1vdmVkLlxyXG4gKlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcclxuICogQHBhcmFtIHtGdW5jdGlvbn0gZm5cclxuICogQHJldHVybiB7RW1pdHRlcn1cclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcblxyXG5FbWl0dGVyLnByb3RvdHlwZS5vbmNlID0gZnVuY3Rpb24oZXZlbnQsIGZuKXtcclxuICBmdW5jdGlvbiBvbigpIHtcclxuICAgIHRoaXMub2ZmKGV2ZW50LCBvbik7XHJcbiAgICBmbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xyXG4gIH1cclxuXHJcbiAgb24uZm4gPSBmbjtcclxuICB0aGlzLm9uKGV2ZW50LCBvbik7XHJcbiAgcmV0dXJuIHRoaXM7XHJcbn07XHJcblxyXG4vKipcclxuICogUmVtb3ZlIHRoZSBnaXZlbiBjYWxsYmFjayBmb3IgYGV2ZW50YCBvciBhbGxcclxuICogcmVnaXN0ZXJlZCBjYWxsYmFja3MuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBldmVudFxyXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmblxyXG4gKiBAcmV0dXJuIHtFbWl0dGVyfVxyXG4gKiBAYXBpIHB1YmxpY1xyXG4gKi9cclxuXHJcbkVtaXR0ZXIucHJvdG90eXBlLm9mZiA9XHJcbkVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUxpc3RlbmVyID1cclxuRW1pdHRlci5wcm90b3R5cGUucmVtb3ZlQWxsTGlzdGVuZXJzID1cclxuRW1pdHRlci5wcm90b3R5cGUucmVtb3ZlRXZlbnRMaXN0ZW5lciA9IGZ1bmN0aW9uKGV2ZW50LCBmbil7XHJcbiAgdGhpcy5fY2FsbGJhY2tzID0gdGhpcy5fY2FsbGJhY2tzIHx8IHt9O1xyXG5cclxuICAvLyBhbGxcclxuICBpZiAoMCA9PSBhcmd1bWVudHMubGVuZ3RoKSB7XHJcbiAgICB0aGlzLl9jYWxsYmFja3MgPSB7fTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLy8gc3BlY2lmaWMgZXZlbnRcclxuICB2YXIgY2FsbGJhY2tzID0gdGhpcy5fY2FsbGJhY2tzWyckJyArIGV2ZW50XTtcclxuICBpZiAoIWNhbGxiYWNrcykgcmV0dXJuIHRoaXM7XHJcblxyXG4gIC8vIHJlbW92ZSBhbGwgaGFuZGxlcnNcclxuICBpZiAoMSA9PSBhcmd1bWVudHMubGVuZ3RoKSB7XHJcbiAgICBkZWxldGUgdGhpcy5fY2FsbGJhY2tzWyckJyArIGV2ZW50XTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLy8gcmVtb3ZlIHNwZWNpZmljIGhhbmRsZXJcclxuICB2YXIgY2I7XHJcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBjYWxsYmFja3MubGVuZ3RoOyBpKyspIHtcclxuICAgIGNiID0gY2FsbGJhY2tzW2ldO1xyXG4gICAgaWYgKGNiID09PSBmbiB8fCBjYi5mbiA9PT0gZm4pIHtcclxuICAgICAgY2FsbGJhY2tzLnNwbGljZShpLCAxKTtcclxuICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgfVxyXG4gIHJldHVybiB0aGlzO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIEVtaXQgYGV2ZW50YCB3aXRoIHRoZSBnaXZlbiBhcmdzLlxyXG4gKlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcclxuICogQHBhcmFtIHtNaXhlZH0gLi4uXHJcbiAqIEByZXR1cm4ge0VtaXR0ZXJ9XHJcbiAqL1xyXG5cclxuRW1pdHRlci5wcm90b3R5cGUuZW1pdCA9IGZ1bmN0aW9uKGV2ZW50KXtcclxuICB0aGlzLl9jYWxsYmFja3MgPSB0aGlzLl9jYWxsYmFja3MgfHwge307XHJcbiAgdmFyIGFyZ3MgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSlcclxuICAgICwgY2FsbGJhY2tzID0gdGhpcy5fY2FsbGJhY2tzWyckJyArIGV2ZW50XTtcclxuXHJcbiAgaWYgKGNhbGxiYWNrcykge1xyXG4gICAgY2FsbGJhY2tzID0gY2FsbGJhY2tzLnNsaWNlKDApO1xyXG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IGNhbGxiYWNrcy5sZW5ndGg7IGkgPCBsZW47ICsraSkge1xyXG4gICAgICBjYWxsYmFja3NbaV0uYXBwbHkodGhpcywgYXJncyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZXR1cm4gdGhpcztcclxufTtcclxuXHJcbi8qKlxyXG4gKiBSZXR1cm4gYXJyYXkgb2YgY2FsbGJhY2tzIGZvciBgZXZlbnRgLlxyXG4gKlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcclxuICogQHJldHVybiB7QXJyYXl9XHJcbiAqIEBhcGkgcHVibGljXHJcbiAqL1xyXG5cclxuRW1pdHRlci5wcm90b3R5cGUubGlzdGVuZXJzID0gZnVuY3Rpb24oZXZlbnQpe1xyXG4gIHRoaXMuX2NhbGxiYWNrcyA9IHRoaXMuX2NhbGxiYWNrcyB8fCB7fTtcclxuICByZXR1cm4gdGhpcy5fY2FsbGJhY2tzWyckJyArIGV2ZW50XSB8fCBbXTtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBDaGVjayBpZiB0aGlzIGVtaXR0ZXIgaGFzIGBldmVudGAgaGFuZGxlcnMuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBldmVudFxyXG4gKiBAcmV0dXJuIHtCb29sZWFufVxyXG4gKiBAYXBpIHB1YmxpY1xyXG4gKi9cclxuXHJcbkVtaXR0ZXIucHJvdG90eXBlLmhhc0xpc3RlbmVycyA9IGZ1bmN0aW9uKGV2ZW50KXtcclxuICByZXR1cm4gISEgdGhpcy5saXN0ZW5lcnMoZXZlbnQpLmxlbmd0aDtcclxufTtcclxuIl19
