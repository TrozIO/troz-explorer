var React = require('react');
var Loader = require('./common/loader.js');
var ProjectStore = require('../stores/ProjectStore');
var AppStateStore = require('../stores/AppStateStore');
var Explorer = require('./explorer/index.js');

function getProjectState() {
  return {
    project: ProjectStore.getProject(),
    app: AppStateStore.getState()
  };
}

var App = React.createClass({

	componentDidMount: function() {
    ProjectStore.addChangeListener(this._onChange);
    AppStateStore.addChangeListener(this._onChange);

    var refreshInput = document.getElementById("refreshInput");
    var refreshTimer = document.getElementById("refreshTimer");
    var refreshBtn = document.getElementById("refreshBtn");
    var runQueryBtn = document.getElementById("run-query");
    var autorefreshActive = false;
    var refreshInterval = null;
    refreshTimer.style.display = 'none';
    refreshBtn.addEventListener('click', function(){
      if (autorefreshActive){
        /* TURN OFF */
        /* first : clear autorefresh interval */
        autorefreshActive = false;
        if (refreshInterval != null) {
          clearInterval(refreshInterval);
          refreshInterval = null;
        }
        /* second : hide timer */
        refreshTimer.innerHTML = "Refreshing...";
        refreshTimer.style.display = "none";
        /* third : reset button of autorefresh */
        refreshBtn.innerHTML = "Set AutoRefresh";
        refreshBtn.classList.remove("btn-danger");
        refreshBtn.classList.add("btn-success");
        /* fourth : enable input */
        refreshInput.removeAttribute('disabled');
      } else {
        /* TURN ON */
        /* validation : check if input is not empty */
        if ((refreshInput.value == "") || isNaN(refreshInput.value)) return false;
        /* first : disable input */
        autorefreshActive = true;
        refreshInput.setAttribute('disabled', 'disabled');
        /* second : reset button of autorefresh */
        refreshBtn.innerHTML = "Cancel AutoRefresh";
        refreshBtn.classList.remove("btn-success");
        refreshBtn.classList.add("btn-danger");
        /* fourth : start timer */
        var intervalCounter = 0;
        refreshTimer.innerHTML = "Refreshing in: "+(refreshInput.value-intervalCounter)+" Seconds";
        refreshTimer.style.display = "block";
        /* third : start interval */
        refreshInterval = setInterval(function(){
          /* fifth : countdown to refresh */
          intervalCounter++;
          if ((refreshInput.value - intervalCounter) <= 0){
            refreshTimer.innerHTML = "Refreshing...";
            /* sixth : force run query */
            runQueryBtn.click();
            /* seventh : reset timer */
            intervalCounter = -1;
          } else {
            refreshTimer.innerHTML = "Refreshing in: "+(refreshInput.value-intervalCounter)+" Seconds";
          }
        }, 1000);
      }
    });
	},

  componentWillUnmount: function() {
    ProjectStore.removeChangeListener(this._onChange);
    AppStateStore.addChangeListener(this._onChange);
  },

	getInitialState: function() {
		return getProjectState();
	},

  render: function () {
    return (
    	<div id="keen-explorer">
    		<Loader visible={this.state.project.loading || !this.state.app.ready} additionalClasses="app-loader" />
        <Explorer project={this.state.project}
                  client={this.props.client}
                  persistence={this.props.persistence} />
      </div>
    );
  },

  _onChange: function() {
    this.setState(getProjectState());
  }

});

module.exports = App;
