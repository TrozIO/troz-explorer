var gulp = require('gulp');
var pkg = require('./package.json');
var path = require('path');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var less = require('gulp-less');
var prefix = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var connect = require('gulp-connect');
var browserify = require('browserify');
var stringify = require('stringify');
var source = require('vinyl-source-stream');
var historyApiFallback = require('connect-history-api-fallback');
var mochaPhantomJS = require('gulp-mocha-phantomjs');
var mochaSelenium = require('gulp-mocha-selenium');
var prompt = require('gulp-prompt');
var replace = require('gulp-replace');
const fileExists = require('file-exists');

// **********************
// Build Config
// **********************

var buildConfig = {
  buildName: 'troz-explorer'
};

// **********************
// Server
// **********************

gulp.task('generate', function () {
  if(fileExists.sync('./index.html')){
    console.log("FILE INDEX ALREADY EXISTS");
    return false;
  }
  return gulp.src('./template/index.html')
    .pipe(prompt.prompt([
      {
        type: 'input',
        name: 'projectId',
        message: 'Project ID?'
      },
      {
        type: 'input',
        name: 'readKey',
        message: 'Read Key?'
      },
      {
        type: 'input',
        name: 'masterKey',
        message: 'Master Key?'
      },
      {
        type: 'input',
        name: 'host',
        message: 'Host?',
        default: 'api.troz.io'
      },
      {
        type: 'input',
        name: 'protocol',
        message: 'Protocol?',
        default: 'https'
      },
      {
        type: 'input',
        name: 'version',
        message: 'Version?',
        default: '2.0'
      }
    ], function(res){
      gulp.src('./template/index.html')
        .pipe(replace('${PROJECT_ID}', res.projectId))
        .pipe(replace('${READ_KEY}', res.readKey))
        .pipe(replace('${MASTER_KEY}', res.masterKey))
        .pipe(replace('${HOST}', res.host))
        .pipe(replace('${PROTOCOL}', res.protocol))
        .pipe(replace('${VERSION}', res.version))
        .pipe(gulp.dest('./'));
    }))
});

gulp.task('connect', function () {
  connect.server({
    root: [__dirname, 'demo', 'dist'],
    port: process.env.PORT || 8081,
    middleware: function(connect, opt) {
      return [ historyApiFallback ];
    }
  });
});

// ********************
// Testing
// ********************

gulp.task('test:functional', function () {
  return gulp.src('test/functional/index.js', {read: false})
    .pipe(mochaSelenium({
      browserName: 'chrome',
      reporter: 'spec',
      useChaining: true,
      timeout: 30e3
    }));
});

gulp.task('test:unit:clean', function() {
  return gulp.src('./test/unit/build', { read: false })
    .pipe(clean());
});

gulp.task('test:unit:build', function () {
  return browserify('./test/unit/index.js', {
      insertGlobals: true,
      debug: true
    })
    .transform(stringify(['.html']))
    .bundle()
    .pipe(source('browserified_tests.js'))
    .pipe(gulp.dest('./test/unit/build'));
});

gulp.task('test:unit:run', function () {
  return gulp.src('./test/unit/index.html')
    .pipe(mochaPhantomJS());
});

// ********************
// Task groups
// ********************

gulp.task('test:build', function(callback) {
  runSequence('test:unit:clean',
              'test:unit:build',
              callback);
});

gulp.task('test:unit', function(callback) {
  runSequence('test:build',
              'test:unit:run',
              callback);
});

// ********************
// Main tasks
// ********************

gulp.task('default', ['test:unit']);
